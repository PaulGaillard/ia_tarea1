# Tarea 1 Inteligencia Artificial

## Uso de la aplicación

Para ejecutar el programa, se debe ejecutar el fichero tarea1.jar ingresando el comando:
```
java -jar tarea1.jar
```
Se puede también ejecuta el fichero tarea1.java con un editor como eclipse. Los 4 algoritmos (búsqueda por profundidad, búsqueda Greedy, búsqueda con costo uniforme y búsqueda A*) se ejecutarán directamente para encontrar una ruta entre A y H.
