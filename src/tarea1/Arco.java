package tarea1;
/**
 * Clase de las caracterisas de las ramas
 * @author Paul Gaillard
 *
 */
public class Arco {
	private Node sucesor;
	private int costo;
	
	/**
	 * Crear un arco
	 * @param n nodo del arco
	 * @param val costo del arco
	 */
	public Arco( Node n, int val ){ 
		this.sucesor = n; 
		this.costo = val;
	}
	
	/**
	 * Obtener el sucesor del arco
	 * @return el sucesor
	 */ 
	public Node getSucesor(){ 
		return this.sucesor;
	}
	
	/**
	 * Obtener el costo del arco
	 * @return
	 */
	public int getVal(){ 
		return this.costo;
	}
}
