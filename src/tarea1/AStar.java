package tarea1;

import java.util.LinkedList;
/**
 * Clase de la lógica de la búsqueda A*
 * @author Paul Gaillard
 *
 */
public class AStar {
	static LinkedList<Node> camino = new LinkedList<Node>();
	static Node origen;
	static Node dest;
	
	/**
	 * Crear la clase
	 * @param a nodo origen
	 * @param h nodo destino
	 */
	public AStar(Node a, Node h){
		origen = a;
		dest = h;
		new Frontera(a.getHeuristic(),new NodoVisitado(a, null));
	}

	/**
	 * Método de ejecución del algoritmo.
	 */
	public void execute(){
		NodoExpandido.clearList();
		
		//Hasta que haya nodos en la frontera continuamos la búsqueda.
		while(Frontera.getSize() != 0){
			//Se obtene el nodo con el costo (f(n)=g(n)+h(n)) minimal 
			Frontera nodef = Frontera.getMinNodeFronteraA();
			int cn = nodef.getVal();
			NodoVisitado n = nodef.getNodeFrontera();
			
			//Registro o modificación de nodo para estadísticas de nodo extendido
			NodoExpandido ntest = NodoExpandido.NodeExist(n.getNode());
			if(ntest != null){
				  ntest.AumentoVal();
			  } else {
				  new NodoExpandido(n.getNode());
			  }
			
			//Actualización de los nodos visitados y en frontera
			new Visitados(cn,n);
			Frontera.remove(nodef);
			
			//Si el nodo es el destino, se termina la búqueda
			if(n.getNode() == dest){
				break;
			} else {
				//Se añade los sucesores (hijos) a la tabla frontera con el costo del nodo desde el origen y su heurístico
				LinkedList<Arco> succesor = n.getNode().getSucesor();
				for(Arco next : succesor){
					new Frontera(cn+next.getVal()+next.getSucesor().getHeuristic()-n.getNode().getHeuristic(),new NodoVisitado(next.getSucesor(),n.getNode()));
				}
			}
		}
		
		//Si encontramos una solución
		if(Visitados.containsNode(dest)){
			//Se muestra el camino 
			String camino = "";
			Visitados mejorpadre = Visitados.getMejorPadre(dest);//para obtener el padre con el costo minimo
			Node padre = mejorpadre.getNodeVisitado().getNode();
			int costo = mejorpadre.getVal();
			
			while(padre != null){
				camino = padre.getId() + " " + camino;
				mejorpadre = Visitados.getMejorPadre(padre);
				padre = mejorpadre.getNodeVisitado().getNodePadre();
			}
			System.out.println("Camino: " + camino);
			
			//Se muestra el costo del camino
			System.out.println("Costo camino: " + costo);
			
			//Prueba si la solución es optima
			if(costo == 18){
				System.out.println("Solución optima: si");
			} else {
				System.out.println("Solución optima: no");
			}
			
			//Se muestra estadísticas de nodos extendidos
			System.out.println("Cantidad de nodos expandidos: " + Visitados.getSize());
			NodoExpandido.PrintList();
			
		} else {
	        System.out.println("Camino no encontrado !");
		}
		
	}
}
