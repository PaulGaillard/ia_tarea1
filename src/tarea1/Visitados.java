package tarea1;

import java.util.ArrayList;
/**
 * Clase para conocer, ordenar y caracterizar los nodos visitados de la búsqueda.
 * @author Paul Gaillard
 *
 */
public class Visitados {
	private int val; 
	private NodoVisitado node;
	static ArrayList<Visitados> visitados = new ArrayList<Visitados>(); //lista de los nodos visitados
	
	/**
	 * Crear un nodo visitado
	 * @param v valor para ordenar el nodo
	 * @param n el nodo
	 */
	public Visitados(int v, NodoVisitado n){
		this.val = v;
		this.node = n;
		visitados.add(this);
	}
	
	/**
	 * Obtener el valor del nodo
	 * @return el valor
	 */
	public int getVal(){
		return this.val;
	}
	
	/**
	 * Obtener el nodo de un NodoVisitado
	 * @return el nodo
	 */
	public NodoVisitado getNodeVisitado(){
		return this.node;
	}
	
	/**
	 * Obtener el nodo que tiene el valor minimal
	 * @return el nodo que tiene el valor minimal
	 */
	public static Visitados getMinNodeVisitadoA(){
		Visitados nodemin = null;
		for(Visitados node : visitados){
			if(nodemin == null || nodemin.getVal()>node.getVal()){
				nodemin = node;
			}
		}
		return nodemin;
	}
	
	/**
	 * Saber hay un nodo en la lista 
	 * @param n el nodo a probar
	 * @return si el nodo existe en la lista
	 */
	public static boolean containsNode(Node n){
		for(Visitados node : visitados){
			if(node.getNodeVisitado().getNode()==n){
				return true;
			}
		}
		return false;
	}
	/**
	 * Obtener el valor de un nodo de manera static
	 * @param n el nodo
	 * @return la valor
	 */
	public static int getValue(Node n){
		for(Visitados node : visitados){
			if(node.getNodeVisitado().getNode()==n){
				return node.val;
			}
		}
		return 0;
	}
	
	/**
	 * Obtener el mejor padre -> el padre que tiene el costo minima para reconstruir el camino 
	 * @param hijo el hijo
	 * @return el mejorpadre
	 */
	public static Visitados getMejorPadre(Node hijo){
		Visitados mejorpadre = null;
		for(Visitados node : visitados){
			if(node.getNodeVisitado().getNode()==hijo){
				
				if(mejorpadre==null || mejorpadre.getVal()>node.getVal()){
					mejorpadre = node;
				}
			}
		}
		return mejorpadre;
	}
	
	/**
	 * Obtener el tamaño de la lista de los nodos visitados
	 * @return el tamaño
	 */
	public static int getSize(){
		return visitados.size();
	}
	
	/**
	 * Reiniciar la lista
	 */
	public static void reinit(){
		visitados.clear();
	}
}
