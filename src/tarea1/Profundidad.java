package tarea1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
/**
 * Clase de la lógica de la búsqueda por profundidad
 * @author Paul Gaillard
 *
 */
public class Profundidad {
	static boolean find = false;
	static ArrayList<Arco> camino = new ArrayList<Arco>();
	static Node origen;
	static Node dest;
	static int cantidadNodos = 0;
	
	/**
	 * Crear la clase
	 * @param a nodo de origen
	 * @param h nodo de destino
	 */
	public Profundidad(Node a, Node h){
		origen = a;
		dest = h;
	}
	
	/**
	 * Método de ejecución del algoritmo.
	 */
	public void execute(){
		new NodoExpandido(origen);
		//inicio de la búsqueda
		busquedaProfundidad(origen);
		
		if(camino.isEmpty()){
	        System.out.println("Camino no encontrado !");
		} else {
			//Se muestra el camino
			Arco arcOrigin = new Arco(origen,0);
			camino.add(arcOrigin);
			Collections.reverse(camino);
	    	System.out.print("Camino: ");
	    	
	    	//Se muestra el costo del camino
	    	int costoCamino = 0;
	        for(Arco arc : camino){
	        	System.out.print(arc.getSucesor().getId() + " ");
	        	costoCamino += arc.getVal();
	        }
	        System.out.println();
	        System.out.println("Costo camino: " + costoCamino);
	        
	        //Prueba si la solución es optima
	        if(costoCamino == 18){
				System.out.println("Solución optima: si");
			} else {
				System.out.println("Solución optima: no");
			}
	        
	        //Se muestra estadísticas de nodos extendidos
	        cantidadNodos++;
	        System.out.println("Cantidad de nodos expandidos: " + cantidadNodos);
	        NodoExpandido.PrintList();
		}
	}
	
	/**
	 * Método de búsqueda recursiva
	 * @param nodo el nodo actual
	 */
	public static void busquedaProfundidad(Node nodo)
	{
	  LinkedList<Arco> list = null;
	  
	  //Si el nodo no tiene sucesor, estamos al final de un rama
	  if( !nodo.hasSucesor() ){
	    return;                 
      } else {
        list = nodo.getSucesor(); 
      }
	  //Bucle en cada sucesor del nodo
	  for( Arco next : list ){
		  if(!find){
			  //incrementando el número de nudos extendidos
			  cantidadNodos++;
			  
			  //Registro o modificación de nodo para estadísticas de nodo extendido
			  NodoExpandido ntest = NodoExpandido.NodeExist(next.getSucesor());
			  if(ntest != null){
				  ntest.AumentoVal();
			  } else {
				  new NodoExpandido(next.getSucesor());
			  }
			  //si el sucesor no es el destino, comenzamos de nuevo con el nodo hijo sino terminamos la búsqueda
			  if(next.getSucesor()!= dest){
		           busquedaProfundidad( next.getSucesor());
		           if(find){
		        	   camino.add(next);
		           }
			  } else {
				  find=true;
				  camino.add(next);
				  break;
			  }
		  }
	  }
	}
}
