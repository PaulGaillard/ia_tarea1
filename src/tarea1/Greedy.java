package tarea1;

import java.util.LinkedList;
/**
 * Clase de la lógica de la búsqueda Greedy
 * @author Paul Gaillard
 *
 */
public class Greedy {
	static LinkedList<Node> camino = new LinkedList<Node>();
	static Node origen;
	static Node dest;
	/**
	 * Crear la clase
	 * @param a nodo origen
	 * @param h nodo destino
	 */
	public Greedy(Node a, Node h){
		origen = a;
		dest = h;
		new Frontera(a.getHeuristic(),new NodoVisitado(a, null));
	}
	
	/**
	 * Obtener el costo del camino entre dos nodos
	 * @param padre nodo padre
	 * @param hijo nodo hijo
	 * @return el costo
	 */
	private int getCosto(Node padre, Node hijo){
		LinkedList<Arco> list = padre.getSucesor();
		for(Arco arc : list){
			if(arc.getSucesor()== hijo){
				return arc.getVal();
			}
		}
		return 0;
	}
	
	/**
	 * Método de ejecución del algoritmo.
	 */
	public void execute(){
		NodoExpandido.clearList();
		
		//Hasta que haya nodos en la frontera continuamos la búsqueda.
		while(Frontera.getSize() != 0){
			//Se obtene el nodo con la heuristica minimal
			Frontera nodef = Frontera.getMinNodeFronteraA();
			int cn = nodef.getVal();
			NodoVisitado n = nodef.getNodeFrontera();
			
			//Registro o modificación de nodo para estadísticas de nodo extendido
			NodoExpandido ntest = NodoExpandido.NodeExist(n.getNode());
			if(ntest != null){
				  ntest.AumentoVal();
			  } else {
				  new NodoExpandido(n.getNode());
			  }
			
			//Actualización de los nodos visitados y en frontera
			new Visitados(cn,n);
			Frontera.remove(nodef);
			
			//Si el nodo es el destino, se termina la búqueda
			if(n.getNode() == dest){
				break;
			} else {
				//Se añade los sucesores (hijos) a la tabla frontera con la heurística si ya no existen en la tabla visitados
				LinkedList<Arco> succesor = n.getNode().getSucesor();
				for(Arco next : succesor){
					if(!Visitados.containsNode(next.getSucesor())){
						new Frontera(next.getSucesor().getHeuristic(),new NodoVisitado(next.getSucesor(),n.getNode()));
					}
				}
			}
		}
		
		//Si encontramos una solución
		if(Visitados.containsNode(dest)){
			//Se muestra el camino y se calcula el costo total del camino
			String camino = "";
			int costo = 0;
			Visitados mejorpadre = Visitados.getMejorPadre(dest);//para obtener el padre con el costo minimo
			Node padre = mejorpadre.getNodeVisitado().getNode();
			costo+=getCosto(padre,dest);
			while(padre != null){
				camino = padre.getId() + " " + camino;
				mejorpadre = Visitados.getMejorPadre(padre);
				padre = mejorpadre.getNodeVisitado().getNodePadre();
				if(mejorpadre.getNodeVisitado().getNode()!=null && padre!=null){
					costo+=getCosto(padre,mejorpadre.getNodeVisitado().getNode());
				}
			}
			System.out.println("Camino: " + camino);
			System.out.println("Costo camino: " + costo);
			
			//Prueba si la solución es optima
			if(costo == 18){
				System.out.println("Solución optima: si");
			} else {
				System.out.println("Solución optima: no");
			}
			
			//Se muestra estadísticas de nodos extendidos
			System.out.println("Cantidad de nodos expandidos: " + Visitados.getSize());
			NodoExpandido.PrintList();
		} else {
	        System.out.println("Camino no encontrado !");
		}
		
	}
}
