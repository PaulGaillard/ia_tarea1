package tarea1;
/**
 * Clase para caracterizar los nodos visitados.
 * @author Paul Gaillard
 *
 */
public class NodoVisitado {
	private Node node;
	private Node nodePadre;
	
	/**
	 * Crear un nodo visitado
	 * @param n nodo visitado
	 * @param padre nodo padre del nodo visitado
	 */
	public NodoVisitado(Node n, Node padre){
		this.node = n;
		this.nodePadre = padre;
	}
	/**
	 * Obtener el nodo
	 * @return el nodo
	 */
	public Node getNode(){
		return this.node;
	}
	
	/**
	 * Obtener el nodo padre
	 * @return el nodo padre
	 */
	public Node getNodePadre(){
		return this.nodePadre;
	}
}
