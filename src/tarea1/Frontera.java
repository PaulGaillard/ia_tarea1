package tarea1;

import java.util.ArrayList;
/**
 * Clase para conocer, ordenar y caracterizar los nodos en la frontera de la búsqueda.
 * @author Paul Gaillard
 *
 */
public class Frontera {
	private int val; 
	private NodoVisitado node;
	static ArrayList<Frontera> frontera = new ArrayList<Frontera>(); //lista de los nodos en frontera
	
	/**
	 * Crear un nodo de frontera
	 * @param v valor para ordenar el nodo
	 * @param n el nodo
	 */
	public Frontera(int v, NodoVisitado n){
		this.val = v;
		this.node = n;
		frontera.add(this);
	}
	
	/**
	 * Obtener el valor del nodo
	 * @return el valor
	 */
	public int getVal(){
		return this.val;
	}
	
	/**
	 * Modificar el valor del nodo
	 * @param val nuevo valor
	 */
	public void updateVal(int val){
		this.val = val;
	}
	
	/**
	 * Modificar el valor de un nodo de manera static 
	 * @param val nuevo valor
	 * @param n nodo a cambiar
	 */
	public static void updateVal(int val, Node n){
		for(Frontera node : frontera){
			if(node.getNodeFrontera().getNode()==n){
				node.val = val;
			}
		}
	}
	
	/**
	 * Obtener el nodo 
	 * @return el nodo
	 */
	public NodoVisitado getNodeFrontera(){
		return this.node;
	}
	
	/**
	 * Obtener el nodo que tiene el valor minimal
	 * @return el nodo que tiene el valor minimal
	 */
	public static Frontera getMinNodeFronteraA(){
		Frontera nodemin = null;
		for(Frontera node : frontera){
			if(nodemin == null || nodemin.getVal()>node.getVal()){
				nodemin = node;
			}
		}
		return nodemin;
	}
	
	/**
	 * Saber hay un nodo en la lista 
	 * @param n el nodo a probar
	 * @return si el nodo existe en la lista
	 */
	public static boolean containsNode(Node n){
		for(Frontera node : frontera){
			if(node.getNodeFrontera().getNode()==n){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Obtener el objeto Frontera de un nodo de manera static
	 * @param n el nodo
	 * @return el objeto Frontera
	 */
	public static Frontera getFronteraNode(Node n){
		for(Frontera node : frontera){
			if(node.getNodeFrontera().getNode()==n){
				return node;
			}
		}
		return null;
	}
	
	/**
	 * Obtener el tamaño de la lista
	 * @return el tamaño
	 */
	public static int getSize(){
		return frontera.size();
	}
	
	/**
	 * Eliminar un nodo de la lista
	 * @param node el nodo a eliminar
	 */
	public static void remove(Frontera node){
		frontera.remove(node);
	}
	
	/**
	 * Reiniciar la lista
	 */
	public static void reinit(){
		frontera.clear();
	}
}