package tarea1;

import java.util.LinkedList;
/**
 * 
 * @author Paul Gaillard
 * Clase de un nodo que contiene sus atributos y métodos.
 */
public class Node {
	
	private String id;
	private int h;
	private LinkedList<Arco> sucesor = new LinkedList<Arco>(); //lista de todos los hijos del nodo
	
	/**
	 * Crear nodo
	 * @param id del nodo
	 * @param h heurística del nodo
	 */
	public Node( String id, int h ){ 
		this.id = id; 
		this.h = h;
	}

	/**
	 * obtener el id del nodo
	 * @return el id del nodo
	 */
	public String getId(){ 
		return id; 
	}
	
	/**
	 * obtener heuristica del nodo
	 * @return heurista
	 */
	public int getHeuristic(){ 
		return h; 
	}
	
	/**
	 * Añadir un sucesor	
	 * @param n el nodo
	 * @param val el costo de la rama
	 */
	public void addSucesor( Node n, int val ){ 
		Arco a = new Arco(n,val);
		sucesor.add(a); 
	}

	/**
	 * Saber si un nodo tiene sucesor
	 * @return el número de sucesor
	 */
	public boolean hasSucesor(){ 
		return sucesor.size() > 0; 
	}
	
	/**
	 * Obtener los sucesores de un nodo
	 * @return los sucesores
	 */
	public LinkedList<Arco> getSucesor(){
		return sucesor; 
	}
	/**
	 * Saber si un nodo es sucesor de un otro
	 * @param n nodo a probar
	 * @return true o false si es sucesor del nodo o no
	 */
	public boolean isSuccesor(Node n){
		for(Arco arc : sucesor){
			if(arc.getSucesor() == n){
				return true;
			}
		}
		return false;
	}

	
}