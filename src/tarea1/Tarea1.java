package tarea1;

/**
 * 
 * @author Paul Gaillard
 * Clase principal del programa que ejecuta los algoritmos de búsqueda.
 */
public class Tarea1 {
	public static void main( String[] args )
	{
	// Creación de los nodos del árbol (con nombres y heurísticos)		
      Node a = new Node( "a",10 );
	  Node b = new Node( "b",7 );
	  Node c = new Node( "c",10 );
	  Node d = new Node( "d",3 );
	  Node e = new Node( "e",1 );
	  Node f = new Node( "f",2 );
	  Node g = new Node( "g",2 );
	  Node h = new Node( "h",0 );
	  
	// Creación de las ramas del árbol (con nodos y costo)
         a.addSucesor( b,11 ); 
         a.addSucesor( c,6 );

         b.addSucesor( d,4 ); 
         b.addSucesor( e,3 );
         c.addSucesor( e,10 );
         c.addSucesor( f,3 );
         
         f.addSucesor( g,1 );
         g.addSucesor( e,4 );
         
         d.addSucesor( h,3 ); 
         e.addSucesor( h,5 );
         
       //Ejecución del algoritmo de búsqueda en profundidad
        Profundidad algo1 = new Profundidad(a,h);
        System.out.println("=========== Búsqueda en profundidad ===========");
        algo1.execute();
        
      //Ejecución del algoritmo de búsqueda por costo uniforme
        CostoUniforme algo2 = new CostoUniforme(a,h);
        System.out.println("\n=========== Búsqueda por costo uniforme ===========");
        algo2.execute();
        
       //Reinicio de las tablas de nodos fronteras y visitados
        Frontera.reinit();
        Visitados.reinit();
        
      //Ejecución del algoritmo de búsqueda Greedy
        Greedy algo3 = new Greedy(a,h);
        System.out.println("\n=========== Búsqueda Greedy ===========");
        algo3.execute();
        
      //Reinicio de las tablas de nodos fronteras y visitados
        Frontera.reinit();
        Visitados.reinit();
        
      //Ejecución del algoritmo de búsqueda A*
        AStar algo4 = new AStar(a,h);
        System.out.println("\n=========== Búsqueda A* ===========");
        algo4.execute();
	}
	
	
}
