package tarea1;

import java.util.ArrayList;
/**
 * 
 * @author Paul Gaillard
 * Clase para conocer y devolver los nodos expandidos durante el algoritmo.
 * 
 */
public class NodoExpandido {
	private Node node;
	private int numerosExp; //números de expansión
	private static ArrayList<NodoExpandido> listNode = new ArrayList<NodoExpandido>();
	
	/**
	 * 
	 * @param n nuevo nodo extandido
	 */
	public NodoExpandido( Node n ){ 
		this.node = n; 
		this.numerosExp = 1;
		listNode.add(this);
	}
	
	/**
	 * @return números de expansión del nodo
	 */
	public int GetVal(){ 
		return this.numerosExp;
	}
	
	/**
	 * @return el node
	 */
	public Node GetNode(){ 
		return this.node;
	}
	
	/**
	 * incrementa el número de expansión de un nodo
	 */
	public void AumentoVal(){ 
		this.numerosExp++;
	}
	
	/**
	 * Reinicio de la lista de nodos expandidos
	 */
	public static void clearList(){
		listNode.clear();
	}
	
	/**
	 * @param n nodo que queremos saber si existe en listNode
	 * @return el nodo expandido si existe o null sino
	 */
	public static NodoExpandido NodeExist(Node n){
		for(NodoExpandido nE : listNode){
			if(n == nE.GetNode()){
				return nE;
			}
		}
		return null;
	}
	
	/**
	 * Muestra la lista de nodos extendidos.
	 */
	public static void PrintList(){
        System.out.println("Nodos expandidos: ");
		for(NodoExpandido nE : listNode){
			System.out.println("Nodo " + nE.GetNode().getId() + " expandido " + nE.GetVal() + " veces;");
		}
	}
}
